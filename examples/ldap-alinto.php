<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<?php
	
	/* Inclusion des librairies utiles */
	$racine = getenv('DOCUMENT_ROOT');
	//require_once($racine.'atlantis2/librairies.php');
	//require_once($racine.'/librairies.php');
	
	
	/*debut du cache*/
	$cache = 'cache/listesDiffusion.htm';
	$expire = time() - 604800 ; // valable une semaine
	 
	/*if(file_exists($cache) && filemtime($cache) > $expire)
	{
			readfile($cache);
	}
	else*/
	{
			ob_start();
?>
<html>
  <!-- InstanceBegin template="/Templates/model.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>
      :: Atlantis - Intranet Prologue :.
    </title>
    <!-- InstanceEndEditable -->
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <!-- InstanceBeginEditable name="head" -->
    <!-- InstanceEndEditable -->
    <link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="js/alinto.js"></script>
  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" height="70">
          <table width="100%" height="70" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <div align="center">
                  <a name="top"></a>
                  <a href="index.php">
                    <img src="images/logo_atlantis_2-1.gif" width="150" height="60" border="0"></a>
                </div>
              </td>
            </tr>
            <tr>
              <td height="4" nowrap background="images/borders/boder_menu_bm2.gif">
                <img src="images/borders/boder_menu_bm2.gif" width="2" height="4">
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table width="100%" height="70" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <div align="center">
                  <!--
                  Bannière Marketing des mails redimenssionnée pour ATLANTIS
                  -->
                  <a href="http://www.prologue.fr/Prologue/Actualite/tabid/214/Default.aspxt.aspx" target="_blank">
                    <img src="http://www.prologue.fr/Portals/0/IMG/png/band_sign_mail1.png" alt="information marketing" width="468" height="60" border="0"></a>
                  
                  <!--
                  Bannière Marketing même taille que dans les mails
                  <a href="http://www.prologue.fr/marketing.php" target="_blank">
                  <img src="http://www.prologue.fr/Portals/0/IMG/png/band_sign_mail1.png" alt="information marketing" width="616" border="0">
                  </a>
                  -->
                  
                  <!--
                  Ancienne bannière d'ATLANTIS
                  <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
                  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
                  width="468" height="60" id="bandeau_prologue" align="center">
                  <param name=movie value="images/flash/bandeau_prologue.swf">
                  <param name=quality value=high>
                  <param name=bgcolor value=#FFFFFF>
                  <embed src="images/flash/bandeau_prologue.swf" quality=high bgcolor=#FFFFFF  width="468" height="60" name="bandeau_prologue" align="center"
                  type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                  </object>
                  -->
                  
                </div>
              </td>
            </tr>
            <tr>
              <td height="4" nowrap background="images/borders/boder_menu_bm2.gif">
                <img src="images/borders/boder_menu_bm2.gif" width="2" height="4">
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td valign="top" bgcolor="#D9D9D9">
          <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="6">
                <img src="images/spacer.gif" width="1" height="1">
              </td>
            </tr>
            <tr>
              <td valign="top">
                <div align="center">
<script type="text/javascript" language="JavaScript1.2" src="js/menu.js"></script>
                </div>
              </td>
            </tr>
            <tr>
              <td height="25" class="txtFooter">
                <p>
                  Derni&egrave;re Mise &agrave; jour
                  le 2007-08-28
                </p>
              </td>
            </tr>
          </table>
        </td>
        <td valign="top">
          <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="25" height="50">
                <img src="images/borders/border_main_tl.gif" width="25" height="50">
              </td>
              <td background="images/borders/border_main_tm.gif" class="txtTitle">
                <!-- InstanceBeginEditable name="title" -->
                Listes
                de Diffusion (e-mail)
                <!-- InstanceEndEditable -->
              </td>
              <td width="25">
                <img src="images/borders/border_main_tr.gif" width="25" height="50">
              </td>
            </tr>
            <tr>
              <td background="images/borders/border_main_ml.gif">
                &nbsp;
              </td>
              <td valign="top" class="txt">
                <!-- InstanceBeginEditable name="main" -->
                <ul>
                  <li>
                  Les listes de diffusion suivantes sont associ&eacute;es&agrave; la nouvelle messagerie Prologue (
                  <strong>@prologue.fr</strong>
                  )</li>
                  <li>Pour envoyer un mail &agrave; l'une d'elles, cliquez directement dessus...</li>
                </ul>
                <p>
                  <br>
                  <strong>Sommaire
                    <br></strong>
                  <strong>
                    <a href="#1">
                      1. Listes de Diffusion Organisationnelles</a>
                    <br></strong>
                  Ces listes  sont
                  hi&eacute;rarchiques et calqu&eacute;es sur l'organigramme de
                  la soci&eacute;t&eacute; ; Elles permettent de joindre une partie
                  ou l'ensemble d'un service.
                  <br>
                  <span class="txt">
                    <strong>
                      <a href="#2">
                        2. Listes de
                        Diffusion Fonctionnelles</a>
                      <br></strong>
                    Ce sont des listes  simples, permettant de joindre un groupe
                    de personne r&eacute;unies autour d'une fonction globale.
                  </span>
                </p>
                <p>
                  &nbsp;
                </p>
                <p class="txtGras">
                  <strong>
                    <a name="1"></a>
                    1. Listes de Diffusion Organisationnelles - Prologue</strong>
                </p>
                
<?php
			
	function liste_diffusion_organisation_alinto($service,$ds,$nbindent) {
			// affiche les listes de diffusion operationnelles (*)
			
			$restriction = array( "sn", "cn", "mail", "mailDrop", "description");
			$dn = "o=prologue.fr,dc=alinto,dc=com";
			if ($service == "") {  // on regarde quelle mailing on utilise
				$mask = "cn=\**";
				//$mask = "cn=o_Prologue_France*";
			}
			else {
				$mask = "cn=$service";
			}
			
			// on fait la recherche
			$group="Groupe";
			$groupalias="Groupe alias";
			$mask="(&(sn=$group)(cn=o_prologue_france*))";
			$sr=ldap_search($ds, $dn, $mask , $restriction);
			$info = ldap_get_entries($ds, $sr);  // on recupere les infos
			echo "<br/><br/>";
			//echo "<ul>";
			echo "<a href='mailto:".$info[0]["mail"][0]."'>".$info[0]["mail"][0]."</a><br/>";
			affiche_contenu_alias_o($info[0], $ds);
			//echo "</ul><br/>";
			
			/*for ($i=0; $i<$info["count"]; $i++)
			{   // pour chaque entrée de la mailing
				echo  "<p>";
				indenter($nbindent);   
				// on affiche le lien de la mailing
				echo  "<a href=mailto:". $info[$i]["mail"][0] . " ";
				if (isset($info[$i]["description"])){
				  echo "title='".$info[$i]["description"][0]."'";
				}
					echo ">".$info[$i]["cn"][0] ."</a><br />\n";
					//echo "Faire afficher ici le contenu du group alias <BR>";
					affiche_contenu_alias($info[$i]);
					
					// *** Code d'origine indenté ***
					// *** A modifier ***
					// si y'a des infos dedans 
					if (isset($info[$i]["maildrop"])) {
						array_multisort($info[$i]["maildrop"], SORT_ASC);	   
						// on affiche ensuite tout ce qui a dedans	
						for($k=0;$k<$info[$i]["maildrop"]["count"];$k++) {
							if  ($info[$i][ "maildrop"][$k][0] != '*' && $info[$i][ "maildrop"][$k][0] != '_'){
								indenter($nbindent);
								afficheNom($info[$i][ "maildrop"][$k],$ds);
							}
						}   // on trie les mails en premier, et les autres mailings ensuite
						for($k=0;$k<$info[$i]["maildrop"]["count"];$k++) {
							if  ($info[$i][ "maildrop"][$k][0] == '*' || $info[$i][ "maildrop"][$k][0] == '_'){
								liste_diffusion_organisation($info[$i][ "maildrop"][$k],$ds,$nbindent+1);
							}
						}
				}
				echo "</p>";
			}*/
			return ;
		}

	function liste_diffusion_fonctionnelle_alinto($ds) {
	/*
	Fonction qui prend en parametres une connexion LDAP et affiche les mailLists commencant par _
	Triées par ordre alphabétique
	*/
		// on prepare la recherche des mailing lists fonctionnelles
		$restriction = array( "cn", "mail", "mailDrop", "description");
   		//$dn = "o=prologue.alinto.com,dc=alinto,dc=com";
   		$dn = "o=prologue.fr,dc=alinto,dc=com";
		$sr=ldap_search($ds, $dn, "cn=f_*" , $restriction);
	
		// on recupere les entrées
		$info = ldap_get_entries($ds, $sr);
		
		//on les affiche
		for ($i=0; $i<$info["count"]; $i++)
	   	{
			echo  "<a href=mailto:". $info[$i]["mail"][0] . " ";
            if (isset($info[$i]["description"]))
            {
              echo "title='".$info[$i]["description"][0]."'";
            }
            echo ">".$info[$i]["cn"][0] ."</a><br />\n";
			
			affiche_contenu_alias_f($info[$i]);
			
			/*
			// si y'a des infos dedans 
			if (isset($info[$i]["maildrop"])) {
				// on trie le tableau et on l'affiche
				array_multisort($info[$i]["maildrop"], SORT_ASC);
				for($k=0;$k<$info[$i]["maildrop"]["count"];$k++) {
					//echo  $info[$i][ "maildrop"][$k] ."<br />";
					afficheNom($info[$i][ "maildrop"][$k],$ds);
					echo "<br />\n";
	       		}
			}*/
			echo "<br>";
		}
		return ;
	}
	
		
	// Eléments d'authentification LDAP
    $ldaprdn  = 'cn=UICB Source,o=prologue.fr,dc=alinto,dc=com'; // DN ou RDN LDAP
    $ldappass = 'UiCbSoUrCe';  // Mot de passe associé

	 // connexion au serveur ldap
	 $ds=ldap_connect("ldap.alinto.net"); 
         ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 10);
	 var_dump($ds);
	 // si on a bien été connecté
	if ($ds) {
		$ldapbind = ldap_bind($ds, $ldaprdn, $ldappass);
                
                var_dump($ldapbind);
                //$err=ldap_err2str( ldap_errno($ds) );
                echo ldap_error($ds);
                exit;
		if ($ldapbind){ 
		// On affiche la liste de diffusion

		//echo "On affiche la liste de diffusion organisationnelle";     
		//liste_diffusion_organisation_alinto("",$ds,0);
?>
<p>
  <a href="#top">
    <img src="images/goto_top.gif" alt="Retour en haut de la page" width="20" height="30" border="0"></a>
  <br>
  <span class="txtGras">
    <strong>
      <a name="2" id="2"></a>
      2. Listes de Diffusion Fonctionnelles - Prologue</strong>
  </span>
</p>
                
<?php
	// on affiche les listes fonctionnelles
    liste_diffusion_fonctionnelle_alinto($ds);
		// on ferme la connexion au LDAP
		ldap_close($ds);	   	
    }
    // si erreur bind
	else {
		echo '<h4>Impossible de le lier sur le serveur LDAP. Veuillez prévenir 2IP</h4>';
	}
	}
	// si erreur connexion LDAP
	else {
		echo '<h4>Impossible de se connecter au serveur LDAP. Veuillez prévenir 2IP</h4>';
	}
?>
		</p>
                <!-- InstanceEndEditable -->
              </td>
              <td background="images/borders/border_main_mr.gif">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td height="25">
                <img src="images/borders/border_main_bl.gif" width="25" height="25">
              </td>
              <td background="images/borders/border_main_bm.gif">
                &nbsp;
              </td>
              <td>
                <img src="images/borders/border_main_br.gif" width="25" height="25">
              </td>
            </tr>
            <tr bgcolor="#D9D9D9">
              <td height="25">
                &nbsp;
              </td>
              <td class="txtFooter">
                Copyright &copy; 2005 Prologue Software - Diffusion
                interne uniquement !
                <br>
                Cette page et l'int&eacute;gralit&eacute; du site intranet Atlantis est la propri&eacute;t&eacute; exclusive
                de Prologue et ne peut &ecirc;tre ni reproduit, ni communiqu&eacute; &agrave; untiers sans l&#8217;autorisation explicite de Prologue.
              </td>
              <td>
                &nbsp;
              </td>
            </tr>
            <tr bgcolor="#D9D9D9">
              <td height="100%">
                <img src="images/spacer.gif" width="4" height="4">
              </td>
              <td>
                <img src="images/spacer.gif" width="4" height="4">
              </td>
              <td>
                <img src="images/spacer.gif" width="4" height="4">
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
  <!-- InstanceEnd -->
</html>

<?php	
			$page = ob_get_contents();
			ob_end_clean();
			
			file_put_contents($cache, $page) ;
			echo $page ;
		}
?>
