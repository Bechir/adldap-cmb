<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
<?php
/*
 * This script is written for use of adLDAP , configuration a connexion to LDAP server, authentication, retreive users , groups
 */
use adLDAP\adLDAP;
include (dirname ( __FILE__ ) . "/../lib/adLDAP/adLDAP.php");
echo ("<pre>\n");


$optionsadVMLAB = array (
		// "account_suffix" => "@vmwarelab.prl",
		"base_dn" => "DC=vmwarelab,DC=prl",
		"domain_controllers" => array (
				"win2008server.vmwarelab.prl" 
		),
		"admin_username" => "CN=administrateur,CN=Users,DC=vmwarelab,DC=prl", // RDN of the Adminn
		"admin_password" => "Prologue2014",
		"real_primarygroup" => true,
		"use_ssl" => false,
		"use_tls" => false,
		"recursive_groups" => true,
		"follow_referrals" => 0,
		"ad_port" => "389",
		"sso" => false 
);

// LDAP shemas for public online LDAP


$optionsadLDAPONLINE = array (
		// "account_suffix" => "@prologue.fr",
		"base_dn" => "dc=example,dc=com",
		"domain_controllers" => array (
				"ldap.forumsys.com" 
		),
		"admin_username" => "cn=read-only-admin,dc=example,dc=com",
		"admin_password" => "password",
		"real_primarygroup" => true,
		"use_ssl" => false,
		"use_tls" => false,
		"recursive_groups" => true,
		"follow_referrals" => 0,
		"ad_port" => "389",
		"sso" => false 
);

// LDAP shemas for public online LDAP

$optionsadPRLinterne = array (
		// "account_suffix" => "@prologue.fr",
		"base_dn" => "DC=prologue,DC=prl",
		"domain_controllers" => array (
				"172.17.160.200" 
		),
		"admin_username" => "CN=SVN Pandore,OU=Comptes d'exploitation,OU=Utilisateurs,DC=prologue,DC=prl",
		"admin_password" => "9yY1McUrP1",
		"real_primarygroup" => true,
		"use_ssl" => false,
		"use_tls" => false,
		"recursive_groups" => true,
		"follow_referrals" => 0,
		"ad_port" => "389",
		"sso" => false 
);

// Load parametres from LDAP forms


$LDAP_shemasVMLAB = array (
		'ObjectClass' => "organizationalPerson",
		'username' => "cn", // used to search user
		'GivenName' => "givenname",
		'Surname' => "X",
		'Email' => "mail",
		'Telephone' => "telephonenumber",
		
		'ObjectClassg' => "group",
		'uniqueidentifierg' => "distinguishedName", // dn of group
		'nameg' => "cn", // nom du groupe
		'Membership' => "member" 
); // les utilisateurs auquel il appartient
   

$LDAP_shemasLDAPONLINE = array (
		'ObjectClass' => "organizationalPerson",
		'username' => "uid", // search_by_attribute_using_an_attribute
		'GivenName' => "sn",
		'Surname' => "X",
		'Email' => "mail",
		'Telephone' => "X",

		
		'ObjectClassg' => "X",
		'uniqueidentifierg' => "X",
		'nameg' => "X",
		'Membership' => "X" 
);

$LDAP_shemasPRLinterne = array (
		'ObjectClass' => "organizationalPerson",
		'username' => "cn", // userprincipalname",//sAMAccountName", // used to search user
		'GivenName' => "givenname",
		'Surname' => "sn",
		'Email' => "mail",
		'Telephone' => "telephonenumber",
				
		'ObjectClassg' => "group",
		'uniqueidentifierg' => "distinguishedName", // dn of group
		'nameg' => "cn", // nom du groupe
		'Membership' => "member" 
); 
// les utilisateur auquel il appartient


////// Selection of the Active Directory Server and Setting variables ////////////////////////

$optionsad = $optionsadPRLinterne;
$LDAP_shemas = $LDAP_shemasPRLinterne;

// ajout d'un select pour spécifier l'attribut d'athentification: (username ou Email)
$Auth_attr=$LDAP_shemasPRLinterne['Email'];//$LDAP_shemasLDAPONLINE['username'];// or $LDAP_shemasVMLAB['Email']
$GIVEN_USERNAME="bt";//"uss";//"user";   

$Given_username = "btalebali@prologue.fr";//"gauss";//;"user2@vmwarelab.prl";
$Given_password = "XXXXXXX";//"password";//;"azerty:2";

try {
	$adldap = new adLDAP ( $optionsad );
    } 
    catch ( adLDAPException $e ) {
	echo $e;
	exit ();
    }

echo "\n ----------------------Test connexion of the given admin user -------------------\n\n";

if (1) 
{
	
	$result = $adldap->authenticate ( $optionsad ['admin_username'], $optionsad ['admin_password'] );
	
	if ($result == true) 
	{
	echo "Authentication  is succeeded \n";
	}
	else
	echo "Authentication is  Failed \n";
}

if (1) {
	// List available username on the Server
		
	$search_res = ($adldap->listavailableusername ( $includeDescription = false, $sorted = true, $LDAP_shemas ['ObjectClass'] ,$LDAP_shemas['username']));

	// Choosing a random entry
	$USERtmp = $search_res [rand ( 0, count ( $search_res ) )];
}
	
	// Print entry's attribute 

if (1) 
{
	$username = $adldap->get_attribute ( $LDAP_shemas ['ObjectClass'],  $LDAP_shemas ['username'], $USERtmp[0], $LDAP_shemas ['username'] );
	$GivenName = $adldap->get_attribute ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['username'], $USERtmp[0], $LDAP_shemas ['GivenName'] );
	$Surname = $adldap->get_attribute ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['username'], $USERtmp[0], $LDAP_shemas ['Surname'] );
	$Email = $adldap->get_attribute ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['username'], $USERtmp[0], $LDAP_shemas ['Email'] );
	$Telephone = $adldap->get_attribute ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['username'], $USERtmp[0], $LDAP_shemas ['Telephone'] );

	echo "\n username           = " . $username [0];
	echo "\n GivenName          = " . $GivenName [0];
	echo "\n Surname            = " . $Surname [0];
	echo "\n Email              = " . $Email [0];
	echo "\n Telephone          = " . $Telephone [0];


}
// ////////////////////////// For Importing Users ////////////////////////////////////////////////////////////////////////////

if (1) {
	$GIVEN_USERNAME = "user";
	
	echo "\n ----------------------Search users using the username " . $GIVEN_USERNAME . "-------------------\n";
	
	$search_res = $adldap->searchusername($includeDescription = false, $sorted = true,  $LDAP_shemas ['ObjectClass'],$LDAP_shemas ['username'],$GIVEN_USERNAME);
	
		
	// For each result, print the user name, mail, surname and givename in a tab
	
	for($i = 0; $i < count ( $search_res ); $i ++) {
		echo ("\n        User " . $i . " :");
		echo $LDAP_shemas ['username'] . " = " . $search_res [$i] . "  ,                ";
		$AUX = $adldap->getallattributeperldapusername ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['username'], $search_res [$i], $LDAP_shemas ['Email'] );
		
		if (array_key_exists ( $LDAP_shemas ['Email'], $AUX [0] )) {
			echo $LDAP_shemas ['Email'] . " = " . ($AUX [0] [$LDAP_shemas ['Email']] [0]);
		} else
			echo $LDAP_shemas ['Email'] . "  not found ";
	}
}


if (1) {
	echo "\n ----------------------Authentication of the  imported User   -----------------------------------\n";
	// on login filed we should introduce username param, then we look the DN of this username, then we make an ldap_bind

	$GIVENDN = $adldap->get_dn_by_username ( $LDAP_shemas ['ObjectClass'], $Auth_attr, $Given_username );
	echo "given DN";
	echo $GIVENDN;
	if (empty ( $GIVENDN )) {
		echo "Authentication failed";
	} else {
		echo "\n DN: " . $GIVENDN;
		$result = $adldap->authenticate ( $GIVENDN, $Given_password );
		if ($result) {
			echo "\n authentication succeeded";
		} else {
			echo "\n authentication failed";
		}
	}
	$adldap->close ();
	
}


// ///////////////////////////////////////////////////////not used ////////////////////////////////////////////////////////

if (0) {
	// //////////////////////////////////////////// For Importing Groupes /////////////////////////////////
	
	echo "\n ----------------------List attributes of groupes: The attribute used here is nameg for example----------------- -------------------\n";
	
	var_export ( $adldap->search_by_attribute_using_an_rdn ( $includeDescription = false, $sorted = true, $LDAP_shemas ['ObjectClassg'], $LDAP_shemas ['nameg'], "*" ) );
}

if (0) {
	echo "\n ----------------------List ALL groupes using the nameg---------------- -------------------\n";
	
	var_export ( $adldap->search_by_attribute_using_an_rdn ( $includeDescription = false, $sorted = true, $LDAP_shemas ['ObjectClassg'], $LDAP_shemas ['nameg'], "*" ) );
}

if (0) {
	
	// // List all users for a specific group Here we take groupe 1
	echo "\n ---------------------- List info about a group  groupe1--------------- -------------------\n";
	
	$USERINFOG = $adldap->getallattributeperldaprdn ( $LDAP_shemas ['ObjectClassg'], $LDAP_shemas ['nameg'], "groupe1" );
	var_export ( $USERINFOG [0] );
	
	echo "\n ----------------------Get a specific attribute for a groupe using nameg=groupe1---------------- -------------------\n";
	echo 'uniqueidentifierg=';
	var_export ( $USERINFOG [0] [$LDAP_shemas ['uniqueidentifierg']] );
	
	echo "\n ----------------------Get a the number of members for a group---------------- -------------------\n";
	echo "Number of users for groupe 1 is " . $USERINFOG [0] [$LDAP_shemas ['Membership']] ['count'];
	
	echo "\n ----------------------List user's RDN for a group's nameg---------------- -------------------\n";
	
	for($a = 0; $a < $USERINFOG [0] [$LDAP_shemas ['Membership']] ['count']; $a ++) {
		
		$USERINFO = $adldap->getallattributeperldaprdn ( $LDAP_shemas ['ObjectClass'], $LDAP_shemas ['uniqueidentifier'], $USERINFOG [0] [$LDAP_shemas ['Membership']] [$a] );
		var_export ( $USERINFO [0] [$LDAP_shemas ['ldaprdnname']] [0] );
	}
}

/*
 * echo "\n ----------------------Getting object Class----------------- -------------------\n"; var_dump( $adldap-> getObjectClass("CN=user1,CN=Users,DC=vmwarelab,DC=prl")); echo "\n"; echo "\n ----------------------search users or groups ----------------------------------\n"; //searching users or groups based on given cn, displayname, samaccountname or sn var_export($adldap-> search($includeDescription = false, $search = "g", $sorted = true)); echo "\n -----------------------get root DSE --------------------------------------------\n"; // get root DSE var_export($adldap-> getRootDse(array('defaultnamingcontext')));
 */

if (0) 

{
	echo "\n -----------------------Test LDAP get-rdn   --------------------------------------------\n";
	
	$filter = "mail=btalebali@prologue.fr";
	$result = ldap_search ( $adldap->getLdapConnection (), $optionsadPRL ['base_dn'], $filter );
	print ("result") ;
	var_export ( $result );
	$count = ldap_count_entries ( $adldap->getLdapConnection (), $result );
	$entry = ldap_first_entry ( $adldap->getLdapConnection (), $result );
	if ($entry) {
		$dn = ldap_get_dn ( $adldap->getLdapConnection (), $entry );
		echo "\nDN: $dn<BR />\n";
	}
	
	// $converted = utf8_encode($dn);
	// echo $converted;
	$handle = ldap_connect ( 'ldap.alinto.net', '389' );
	echo ("\n");
	var_dump ( $handle );
	ldap_set_option ( $handle, LDAP_OPT_PROTOCOL_VERSION, 2 );
	ldap_set_option ( $handle, LDAP_OPT_REFERRALS, 0 );
	define ( LDAP_OPT_DIAGNOSTIC_MESSAGE, 0x0032 );
	ldap_set_option ( NULL, LDAP_OPT_DEBUG_LEVEL, 7 );
	
	var_dump ( $dn );
	$bind = ldap_bind ( $handle, $dn, '0DtSV6y7n' );
	var_dump ( $bind );
	ldap_errno ( $adldap->getLdapConnection () );
	$err = ldap_err2str ( ldap_errno () );
	
	echo "error=";
	var_dump ( $d );
	@ldap_close ( $handle );
	
	// Pour Active Directory, si on utilise le userprincipalname au lieu du samaccountname on peut avoir un rootdn sous la forme : prenom.nom@domaine.fr
}
